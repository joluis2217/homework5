package com.emedinaa.kotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.emedinaa.kotlinapp.adapter.AdapterMusica
import com.emedinaa.kotlinapp.db.BMusica
import com.emedinaa.kotlinapp.db.BMusicaDataBase
import com.emedinaa.kotlinapp.db.BMusicaRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.contracts.ReturnsNotNull

class MainActivity : AppCompatActivity() {

    private lateinit var musicaRepository:BMusicaRepository

    private val adapter : AdapterMusica by lazy {



        AdapterMusica(db() ,onItemAction())


    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ui()
    }


    fun ui (){

        recyclerView.adapter=adapter

        ButtonAgregar.setOnClickListener {

            AgregarPantalla()
        }

    }

    private fun EnviarEdit(item: BMusica) {
        val bundle = Bundle().apply {
            putParcelable("ENTITY", item)
        }
        val intent = Intent(this, EditMusica::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }



    private fun db() : List<BMusica>{
        musicaRepository= BMusicaRepository(BMusicaDataBase(this))


        val musicas:List<BMusica> = musicaRepository.musicas()

        return musicas
    }

    fun onItemAction() : ( musica : BMusica) -> Unit{
        return {
            EnviarEdit(it)

        }

    }
    private fun AgregarPantalla() {
        val intent = Intent(this, AgregarMusica::class.java)
        startActivity(intent)
    }

}