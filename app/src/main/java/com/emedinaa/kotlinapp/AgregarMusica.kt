package com.emedinaa.kotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.emedinaa.kotlinapp.db.BMusica
import com.emedinaa.kotlinapp.db.BMusicaDataBase
import com.emedinaa.kotlinapp.db.BMusicaRepository
import kotlinx.android.synthetic.main.activity_agregar_musica.*

class AgregarMusica : AppCompatActivity() {

    private lateinit var musicaRepository: BMusicaRepository



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_agregar_musica)

        musicaRepository= BMusicaRepository(BMusicaDataBase(this))




         ButtonAgregarMusica.setOnClickListener {


             if(validar()){

                 musicaRepository.addMusica(BMusica(0,EdtNombreAdd.text.toString(),EdtDescripcionAdd.text.toString()))

                 ListarPantalla()
             }


         }



    }


    fun validar() : Boolean {

      if(EdtNombreAdd.text.isEmpty()){

          Toast.makeText(this,"Ingrese el Nombre de la Musica",Toast.LENGTH_LONG)
          return false
      }

        if(EdtDescripcionAdd.text.isEmpty()){

            Toast.makeText(this,"Ingrese el Nombre de la Musica",Toast.LENGTH_LONG)
            return false
        }



        return true
    }
    private fun ListarPantalla() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }





}