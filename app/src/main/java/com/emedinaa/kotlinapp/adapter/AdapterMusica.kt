package com.emedinaa.kotlinapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.kotlinapp.R
import com.emedinaa.kotlinapp.db.BMusica
import kotlinx.android.synthetic.main.fila.view.*

class AdapterMusica (private var MusicaList : List<BMusica>,val itemAction : (item : BMusica) -> Unit)  :
    RecyclerView.Adapter<AdapterMusica.MusicaViewHolder>(){

    inner class  MusicaViewHolder(private val view : View) : RecyclerView.ViewHolder(view){

        fun bind(entity : BMusica){
            view.tvNombre.text=entity.nombre
            view.tvDescripcion.text=entity.descripcion
            view.setOnClickListener {
                itemAction(entity)
            }


        }

    }

    fun update(nMusica : List<BMusica>){
        this.MusicaList=nMusica
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MusicaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fila,parent,false)
        return MusicaViewHolder(view)
    }

    override fun getItemCount(): Int {
   return MusicaList.count()
    }

    override fun onBindViewHolder(holder: MusicaViewHolder, position: Int) {
        holder.bind(MusicaList[position])
    }
}