package com.emedinaa.kotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.emedinaa.kotlinapp.db.BMusica
import com.emedinaa.kotlinapp.db.BMusicaDataBase
import com.emedinaa.kotlinapp.db.BMusicaRepository
import kotlinx.android.synthetic.main.activity_edit_musica.*

class EditMusica : AppCompatActivity() {

    private lateinit var musicaRepository: BMusicaRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_musica)

        populate()
        musicaRepository= BMusicaRepository(BMusicaDataBase(this))


        ButtonEditarMusica.setOnClickListener {



            (intent?.extras?.getParcelable("ENTITY") as? BMusica)?.let {
                    it.nombre= EdtNombreDet.text.toString()
                  it.descripcion=EdtDescripcionDet.text.toString()

                musicaRepository.updateMusica(it)

            }
            ListarPantalla()


        }




        ButtonEliminarMusica.setOnClickListener {
            (intent?.extras?.getParcelable("ENTITY") as? BMusica)?.let {
                musicaRepository.deleteMusica(it)

            }
            ListarPantalla()

        }

    }





    private fun populate(){
        (intent?.extras?.getParcelable("ENTITY") as? BMusica)?.let {

            EdtNombreDet.setText(it.nombre)
            EdtDescripcionDet.setText( it.descripcion)

        }


    }


    private fun ListarPantalla() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }


}