package com.emedinaa.kotlinapp.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class BMusicaDataBase (context: Context): SQLiteOpenHelper(context, DATABASE_NAME,null,DATABASE_VERSION){
    companion object {
        const val DATABASE_VERSION:Int=1
        const val DATABASE_NAME:String="BDMusica"

        const val TABLE_NAME="TB_Musica"
        const val KEY_ID:String= "id"
        const val KEY_NAME:String= "nombre"
        const val KEY_DESC:String= "descripcion"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val sql = ("CREATE TABLE ${TABLE_NAME} ("
                + "${KEY_ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , ${KEY_NAME}  TEXT,"
                + "${KEY_DESC} TEXT )")

        Log.v("CONSOLE","sql $sql")
        db?.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        val sql = "DROP TABLE IF EXISTS ${TABLE_NAME}"
        db?.execSQL(sql)
    }



}
