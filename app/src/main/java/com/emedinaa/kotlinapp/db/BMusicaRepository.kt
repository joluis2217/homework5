package com.emedinaa.kotlinapp.db

import android.content.ContentValues

class BMusicaRepository (val musicaDatabase: BMusicaDataBase){

    fun musicas():List<BMusica>{
        val sqliteDatabase= musicaDatabase.readableDatabase

        val notes:MutableList<BMusica> = mutableListOf()
        val sql= "SELECT  * FROM ${BMusicaDataBase.TABLE_NAME}"
        val cursor= sqliteDatabase.rawQuery(sql,null)
        if(cursor.moveToFirst()){
            do{
                val note= BMusica(cursor.getString(0).toInt(),
                    cursor.getString(1),
                    cursor.getString(2))
                notes.add(note)
            }while(cursor.moveToNext())
        }
        sqliteDatabase.close()
        return notes.toList()
    }

    fun addMusica(musica:BMusica){
        val sqliteDatabase= musicaDatabase.writableDatabase

        val contentValues= ContentValues()
        contentValues.put(BMusicaDataBase.KEY_NAME,musica.nombre)
        contentValues.put(BMusicaDataBase.KEY_DESC,musica.descripcion)

        sqliteDatabase.insert(BMusicaDataBase.TABLE_NAME,null,contentValues)
        sqliteDatabase.close()
    }

    fun deleteMusica(musica:BMusica):Int{
        val sqliteDatabase= musicaDatabase.writableDatabase
        val row= sqliteDatabase.delete(BMusicaDataBase.TABLE_NAME,
            "${BMusicaDataBase.KEY_ID}=?", arrayOf(musica.id.toString()))
        sqliteDatabase.close()
        return row
    }

    fun updateMusica(musica: BMusica):Int{
        val sqliteDatabase= musicaDatabase.writableDatabase
        val contentValues= ContentValues()
        contentValues.put(BMusicaDataBase.KEY_NAME,musica.nombre)
        contentValues.put(BMusicaDataBase.KEY_DESC,musica.descripcion)

        val row= sqliteDatabase.update(BMusicaDataBase.TABLE_NAME,contentValues,
            "${BMusicaDataBase.KEY_ID}=?", arrayOf(musica.id.toString()))
        sqliteDatabase.close()
        return row
    }


}