package com.emedinaa.kotlinapp.db

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BMusica (var id:Int?, var nombre:String?, var descripcion:String?) : Parcelable {



}